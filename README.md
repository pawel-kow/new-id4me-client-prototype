### Client for ID4me Trust Framework and Verified Identity

A new spring-boot based client for the ID4me extensions Trust Framework and Verified Identity.

### This client is currently for testing purposes only.

It only functions with specific settings and the other components from the proof-of-concept.

The other components are:
- https://gitlab.com/dunksten/id4me-intermediate
- https://gitlab.com/dunksten/id4me-trust-anchor
- https://gitlab.com/dunksten/connect2id-docker

#### Build the project

- Make sure that JDK 8 is installed.
- Open a terminal in the base directory of the project.
- Type ```./mvnw clean install -DskipTests```

#### Run the project

- Type ```./mvnw spring-boot:run```

#### The client runs at http://localhost:9000