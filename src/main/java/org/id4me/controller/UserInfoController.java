package org.id4me.controller;

import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.SerializeException;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.sdk.UserInfoErrorResponse;
import com.nimbusds.openid.connect.sdk.UserInfoRequest;
import com.nimbusds.openid.connect.sdk.UserInfoResponse;
import com.nimbusds.openid.connect.sdk.UserInfoSuccessResponse;
import net.minidev.json.JSONObject;
import org.id4me.Service.OIDCClientService;
import org.id4me.model.OIDCClient;
import org.id4me.model.form.RegistrationForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.io.IOException;

@Controller
@SessionAttributes({"RegistrationForm", "accessToken"})
public class UserInfoController {

    private static final Logger logger = LoggerFactory.getLogger(UserInfoController.class);

    @Autowired
    OIDCClientService oidcClientService;

    @GetMapping("/userinfo")
    public String getUserInfo(@ModelAttribute("accessToken") AccessToken accessToken, @ModelAttribute("RegistrationForm") RegistrationForm registrationForm) {
        logger.info(registrationForm.getProvider());
        logger.info(accessToken.getValue());

        OIDCClient oidcClient = oidcClientService.getOIDCClientfromProvider(registrationForm.getProvider());

        UserInfoRequest userInfoReq = new UserInfoRequest(
                oidcClient.getProviderUserInfoURI(),
                (BearerAccessToken) accessToken);


        HTTPResponse userInfoHTTPResp = null;
        try {
            userInfoHTTPResp = userInfoReq.toHTTPRequest().send();
        } catch (SerializeException | IOException e) {
            logger.error(e.getMessage());
        }

        UserInfoResponse userInfoResponse = null;
        try {
            assert userInfoHTTPResp != null;
            userInfoResponse = UserInfoResponse.parse(userInfoHTTPResp);
        } catch (ParseException e) {
            logger.error(e.getMessage());
        }

        if (userInfoResponse instanceof UserInfoErrorResponse) {
            ErrorObject error = ((UserInfoErrorResponse) userInfoResponse).getErrorObject();
            logger.error(error.getCode() + error.getDescription());
        }

        assert userInfoResponse instanceof UserInfoSuccessResponse;
        UserInfoSuccessResponse successResponse = (UserInfoSuccessResponse) userInfoResponse;
        JSONObject claims = successResponse.getUserInfo().toJSONObject();

        logger.info(claims.toJSONString());
        return "success";
    }
}
