package org.id4me.controller;


import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.openid.connect.sdk.*;
import com.nimbusds.openid.connect.sdk.token.OIDCTokens;
import org.id4me.Service.OIDCClientService;
import org.id4me.model.OIDCClient;
import org.id4me.model.form.RegistrationForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

@Controller
@SessionAttributes({"RegistrationForm", "accessToken"})
public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
    @Autowired
    OIDCClientService oidcClientService;

    @GetMapping("/auth")
    public ModelAndView auth(@ModelAttribute("RegistrationForm") RegistrationForm registrationForm) throws MalformedURLException {
        // Generate random state string for pairing the response to the request
        State state = new State();
        // Generate nonce
        Nonce nonce = new Nonce();
        // Specify scope
        Scope scope = Scope.parse("openid");

        String provider = registrationForm.getProvider();

        OIDCClient oidcClient = oidcClientService.getOIDCClientfromProvider(provider);

        ClaimsRequest claimsRequest = new ClaimsRequest();
        claimsRequest.addVerifiedUserInfoClaim(new ClaimsRequest.Entry("name"));

        // Compose the request
        AuthenticationRequest authenticationRequest = new AuthenticationRequest.Builder(
                new ResponseType(ResponseType.Value.CODE),
                scope,
                oidcClient.getCliendId(),
                oidcClient.getRedirectUri())
                .state(state)
                .claims(claimsRequest)
                .purpose("Account holder identification")
                .endpointURI(oidcClient.getProviderAuthorizationURI())
                .build();

        URL authReqURI = authenticationRequest.toURI().toURL();

        logger.info(authReqURI.toString());

        return new ModelAndView("redirect:" + authReqURI);
    }

    @GetMapping("/callback")
    public String callback(@ModelAttribute("RegistrationForm") RegistrationForm registrationForm, HttpServletRequest httpServletRequest, Model model) {

        String uri = httpServletRequest.getRequestURL() + "?" + httpServletRequest.getQueryString();
        logger.info(uri);
        AuthenticationResponse authResp = null;
        try {
            authResp = AuthenticationResponseParser.parse(new URI(uri));
        } catch (ParseException | URISyntaxException e) {
            logger.warn(e.getMessage());
        }

        if (authResp instanceof AuthenticationErrorResponse) {
            ErrorObject error = ((AuthenticationErrorResponse) authResp)
                    .getErrorObject();
            logger.warn(error.getCode(), error.getDescription());
        }

        AuthenticationSuccessResponse successResponse = (AuthenticationSuccessResponse) authResp;

        AuthorizationCode authCode = successResponse.getAuthorizationCode();
        try {
            logger.info(successResponse.getJWTResponse().serialize());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        logger.info(authCode.getValue());

        String provider = registrationForm.getProvider();

        OIDCClient oidcClient = oidcClientService.getOIDCClientfromProvider(provider);

        TokenRequest tokenReq = new TokenRequest(
                oidcClient.getProviderTokenURI(),
                new ClientSecretBasic(oidcClient.getCliendId(),
                        oidcClient.getClientSecret()),
                new AuthorizationCodeGrant(authCode, oidcClient.getRedirectUri()));

        HTTPResponse tokenHTTPResp = null;
        try {
            tokenHTTPResp = tokenReq.toHTTPRequest().send();
        } catch (SerializeException | IOException e) {
            logger.error(e.getMessage());
        }

// Parse and check response
        TokenResponse tokenResponse = null;
        try {
            assert tokenHTTPResp != null;
            tokenResponse = OIDCTokenResponseParser.parse(tokenHTTPResp);
        } catch (ParseException e) {
            logger.error(e.getMessage());
        }

        if (tokenResponse instanceof TokenErrorResponse) {
            ErrorObject error = ((TokenErrorResponse) tokenResponse).getErrorObject();
            logger.error(error.getCode() + error.getDescription());
        }

        AccessTokenResponse accessTokenResponse = (AccessTokenResponse) tokenResponse;
        OIDCTokens tokens = accessTokenResponse.getTokens().toOIDCTokens();
        logger.info(tokens.getAccessToken().toJSONString());
        try {
            logger.info(tokens.getIDToken().getJWTClaimsSet().toJSONObject().toJSONString());
        } catch (java.text.ParseException e) {
            logger.error(e.getMessage());
        }


        model.addAttribute("accessToken", tokens.getAccessToken());

        return "success";
    }
}
