package org.id4me.controller;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.openid.connect.sdk.federation.entities.EntityID;
import com.nimbusds.openid.connect.sdk.federation.entities.EntityStatement;
import com.nimbusds.openid.connect.sdk.federation.entities.FederationMetadataType;
import com.nimbusds.openid.connect.sdk.federation.policy.MetadataPolicy;
import com.nimbusds.openid.connect.sdk.federation.policy.MetadataPolicyEntry;
import com.nimbusds.openid.connect.sdk.federation.policy.language.PolicyViolationException;
import com.nimbusds.openid.connect.sdk.federation.policy.operations.PolicyOperationCombinationValidator;
import com.nimbusds.openid.connect.sdk.federation.trust.ResolveException;
import com.nimbusds.openid.connect.sdk.federation.trust.TrustChain;
import com.nimbusds.openid.connect.sdk.federation.trust.TrustChainResolver;
import com.nimbusds.openid.connect.sdk.federation.trust.TrustChainSet;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Controller
public class ResolvTrustController {

    private static final Logger logger = LoggerFactory.getLogger(ResolvTrustController.class);

    @GetMapping("/trust")
    public String resolvTrustChain(Model model) {
        // The configured federation trust anchor URL
        EntityID trustAnchor = new EntityID("http://localhost:8080");

        // The entity ID of the OpenID provider to resolve
        EntityID openIDProviderEntity = new EntityID("http://localhost:8090");


        TrustChainResolver resolver = new TrustChainResolver(trustAnchor);

        TrustChainSet resolvedChains = null;
        try {
            resolvedChains = resolver.resolveTrustChains(openIDProviderEntity);
        } catch (ResolveException e) {
            // Couldn't resolve a valid trust chain
            logger.error(e.getMessage());
        }

        assert resolvedChains != null;
        TrustChain chain = resolvedChains.getShortest();

        logger.info("Trust anchor:" + chain.getTrustAnchorEntityID().toString());
        logger.info("Entity:" + chain.getLeafSelfStatement().getEntityID().toString());

        MetadataPolicy metadataPolicy = null;
        JSONObject jsonObject = new JSONObject();

        chain.getSuperiorStatements().forEach(element -> {
            logger.info("Chain:" + element.getEntityID().toString());
            logger.info("Metadata_Policy: " + element.getClaimsSet().getMetadataPolicyJSONObject().toJSONString());

            logger.info("Leaf Metadata: " + chain.getLeafSelfStatement().getClaimsSet().toJSONString());
        });


        try {
            metadataPolicy = resolveCombinedMetadataPolicy(chain);
        } catch (ParseException | PolicyViolationException e) {
            e.printStackTrace();
        }
        List<String> metadataTypes = new ArrayList<>();
        assert metadataPolicy != null;
        metadataPolicy.entrySet().forEach(entry -> metadataTypes.add(entry.getKey()));
        MetadataPolicy finalMetadataPolicy = metadataPolicy;
        metadataTypes.forEach(type -> {
            try {
                finalMetadataPolicy.apply(chain.getLeafSelfStatement().getClaimsSet().getMetadata(new FederationMetadataType(type)));
            } catch (PolicyViolationException e) {
                e.printStackTrace();
            }
        });

        model.addAttribute("trust_chain", chain);


        return "success_trust_chain";
    }

    private MetadataPolicy resolveCombinedMetadataPolicy(TrustChain chain) throws ParseException, PolicyViolationException {
        PolicyOperationCombinationValidator combinationValidator = MetadataPolicyEntry.DEFAULT_POLICY_COMBINATION_VALIDATOR;
        List<MetadataPolicy> policies = new LinkedList();
        Iterator var3 = chain.getSuperiorStatements().iterator();

        while (var3.hasNext()) {
            EntityStatement stmt = (EntityStatement) var3.next();
            stmt.getClaimsSet().getMetadataPolicyJSONObject().forEach(
                    (type, policy) -> {

                    }
            );
            JSONObject jsonObject = stmt.getClaimsSet().getMetadataPolicyJSONObject();
            if (jsonObject != null) {
                policies.add(MetadataPolicy.parse(jsonObject));
            }
        }

        return MetadataPolicy.combine(policies, combinationValidator);
    }
}
