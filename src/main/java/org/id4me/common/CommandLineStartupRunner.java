package org.id4me.common;

import org.id4me.repository.OIDCClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;


@Component
public class CommandLineStartupRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(CommandLineStartupRunner.class);

    @Autowired
    OIDCClientRepository oidcClientRepository;

    @Override
    public void run(String... args) {

        logger.info("Application started with command-line arguments: {} . \n To kill this application, press Ctrl + C.", Arrays.toString(args));

        oidcClientRepository.deleteAllByProvider("http://localhost:9090");

    }

}
