package org.id4me.model;

import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.net.URI;

@Entity
public class OIDCClient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String provider;
    private ClientID cliendId;
    private Secret clientSecret;
    private URI redirectUri;
    private URI providerAuthorizationURI;
    private URI providerTokenURI;
    private URI providerUserInfoURI;
    private URI providerJWKSetURI;

    public OIDCClient() {
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public ClientID getCliendId() {
        return cliendId;
    }

    public void setCliendId(ClientID cliendId) {
        this.cliendId = cliendId;
    }

    public Secret getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(Secret clientSecret) {
        this.clientSecret = clientSecret;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public URI getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(URI redirectUri) {
        this.redirectUri = redirectUri;
    }

    public URI getProviderAuthorizationURI() {
        return providerAuthorizationURI;
    }

    public void setProviderAuthorizationURI(URI providerAuthorizationURI) {
        this.providerAuthorizationURI = providerAuthorizationURI;
    }

    public URI getProviderTokenURI() {
        return providerTokenURI;
    }

    public void setProviderTokenURI(URI providerTokenURI) {
        this.providerTokenURI = providerTokenURI;
    }

    public URI getProviderUserInfoURI() {
        return providerUserInfoURI;
    }

    public void setProviderUserInfoURI(URI providerUserInfoURI) {
        this.providerUserInfoURI = providerUserInfoURI;
    }

    public URI getProviderJWKSetURI() {
        return providerJWKSetURI;
    }

    public void setProviderJWKSetURI(URI providerJWKSetURI) {
        this.providerJWKSetURI = providerJWKSetURI;
    }
}
